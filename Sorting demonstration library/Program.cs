﻿using System;
using System.Linq;
using Sorting_demonstration_library.Sorting;
using Sorting_demonstration_library.Sorting.Preparation;
using static System.Console;

namespace Sorting_demonstration_library
{
  class Program
  {
    private const int _arraySize = 100;
    private static bool _isSomeArrayIncorrect = false;
    private static readonly Random _random = new Random();

    static void Main(string[] args)
    {
      Sorting.Base[] sorts = CreateSorts();

      foreach (var sort in sorts)
      {
        WriteLine($"\"{NameOfAlgorithm(sort)}\" sort algorithm.");
        WriteLine();
        
        WriteLine("Source confusing array:");
        DisplaySoursArray(sort.Array);
        WriteLine();
        WriteLine();
        
        sort.Sort();
        WriteLine("Sorting array:");
        DisplaySoursArray(sort.Array);
        WriteLine();
        WriteLine();
        
        WriteLine($"Call method: \"{nameof(Test.IsSequenceCorrect)}\".");
        if (Test.IsSequenceCorrect(sort.Array))
        {
          WriteLine("Sorting array is correct!");
        }
        else
        {
          _isSomeArrayIncorrect = true;
          WriteLine("Sorting array is NOT correct!");
        }

        WriteLine();
        WriteLine();
        WriteLine();
      }
      
      if(_isSomeArrayIncorrect)
        WriteLine("Some sorting incorrect!");

      ReadLine();
    }

    private static string NameOfAlgorithm(Base sort)
    {
      var fullName = sort.ToString();
      var name = fullName.Split('.').Last();
      return name;
    }

    private static void DisplaySoursArray(int[] array)
    {
      WriteLine();
      foreach (var element in array)
        Write($"{element} ");
    }

    private static Base[] CreateSorts()
    {
      return new Sorting.Base[]
      {
        CreateCountingSort(),
        new Heap(CreateRandomArray(_arraySize)),
        new Quick(CreateRandomArray(_arraySize)),
        new Merge(CreateRandomArray(_arraySize)),
        new Bubble(CreateRandomArray(_arraySize)),
        new Selection(CreateRandomArray(_arraySize)),
        new Insertion(CreateRandomArray(_arraySize)),
      };
    }

    private static Counting CreateCountingSort()
    {
      var lowerBound = 0;
      var countingSort = new Counting(CreateRandomArray(_arraySize));
      countingSort.Prepare(new CountingSort(_arraySize, lowerBound, countingSort));
      return countingSort;
    }

    private static int[] CreateRandomArray(int size)
    {
      var array = new int[size];
      FillInWithRandomValues(array);
      return array;
    }

    private static void FillInWithRandomValues(int[] array, int min = 10, int max = 99)
    {
      for (int i = 0; i < array.Length; i++)
      {
        array[i] = _random.Next(min, max + 1);
      }
    }
  }
}