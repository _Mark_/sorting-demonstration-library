﻿namespace Sorting_demonstration_library.Sorting
{
  public abstract class Base
  {
    private readonly int[] _array;

    public int[] Array => _array;

    protected Base(int[] array)
    {
      _array = array;
    }

    public abstract void Sort();

    public Sorting.Base Prepare(Preparation.IPreparation preparation)
    {
      preparation.Prepare();
      return this;
    }

    protected void Swap(int first, int second)
    {
      (Array[first], Array[second]) = (Array[second], Array[first]);
    }
  }
}