﻿namespace Sorting_demonstration_library.Sorting
{
  public class Insertion : Sorting.Base
  {
    public Insertion(int[] array) : base(array) { }

    public override void Sort()
    {
      for (int elementIndex = 1; elementIndex < Array.Length; ++elementIndex)
      {
        var element = Array[elementIndex];
        var sortedIndex = elementIndex - 1;
        while (sortedIndex >= 0 && element <= Array[sortedIndex])
        {
          Array[sortedIndex + 1] = Array[sortedIndex];
          --sortedIndex;
        }
        Array[sortedIndex + 1] = element;
      }
    }
  }
}