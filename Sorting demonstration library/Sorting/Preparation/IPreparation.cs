﻿namespace Sorting_demonstration_library.Sorting.Preparation
{
  public interface IPreparation
  {
    void Prepare();
  }
}