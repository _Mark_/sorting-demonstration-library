﻿using System;

namespace Sorting_demonstration_library.Sorting.Preparation
{
  public class CountingSort : IPreparation
  {
    private readonly int _lenght;
    private readonly int _lowerBound;
    private readonly Counting _countingSort;

    public CountingSort(int lenght, int lowerBound, Sorting.Counting countingSort)
    {
      _lenght = lenght;
      _lowerBound = lowerBound;
      _countingSort = countingSort;
    }

    public void Prepare()
    {
      Array counter = Array.CreateInstance(typeof(int), new[] {_lenght}, new[] {_lowerBound});
      _countingSort.Counter = counter;
      _countingSort.MaxValue = counter.GetUpperBound(0);
    }
  }
}