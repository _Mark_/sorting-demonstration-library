﻿namespace Sorting_demonstration_library.Sorting
{
  public class Merge : Sorting.Base
  {
    public Merge(int[] array) : base(array) { }

    public override void Sort()
    {
      int[] buffer = new int[Array.Length];
      Sort(buffer, 0, Array.Length - 1);
    }

    private void Sort(int[] buffer, int start, int end)
    {
      if (start == end) return;

      int middle = (start + end) / 2;

      Sort(buffer, start, middle);
      Sort(buffer, middle + 1, end);

      int left = start;
      int right = middle + 1;
      int bufferIndex = left;

      while (left <= middle && right <= end)
      {
        if (Array[left] < Array[right])
        {
          buffer[bufferIndex] = Array[left];
          ++left;
        }
        else
        {
          buffer[bufferIndex] = Array[right];
          ++right;
        }

        ++bufferIndex;
      }

      for (int i = left; i <= middle; i++)
      {
        buffer[bufferIndex] = Array[left];
        ++bufferIndex;
      }

      for (int i = right; i <= end; i++)
      {
        buffer[bufferIndex] = Array[right];
        ++bufferIndex;
      }

      for (int i = start; i <= end; i++)
      {
        Array[i] = buffer[i];
      }
    }
  }
}