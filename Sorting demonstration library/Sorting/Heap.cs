﻿namespace Sorting_demonstration_library.Sorting
{
  public class Heap : Sorting.Base
  {
    public Heap(int[] array) : base(array) { }

    public override void Sort()
    {
      RestoreHeapProperty(Array.Length);
      for (int i = Array.Length - 1; i > 0; --i)
      {
        (Array[0], Array[i]) = (Array[i], Array[0]);
        RestoreHeapProperty(i);
      }
    }

    private void RestoreHeapProperty(int quantityElements)
    {
      for (int i = 1; i < quantityElements; ++i)
      {
        var index = i;
        var parent = ParentIndexFor(index);
        while (index > 0 && Array[index] > Array[parent])
        {
          Swap(index, parent);
          index = parent;
          parent = ParentIndexFor(index);
        }
      }
    }

    private static int ParentIndexFor(int index)
    {
      int result = (index - 1) / 2;
      return result;
    }
  }
}