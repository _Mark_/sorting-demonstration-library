﻿using System;

namespace Sorting_demonstration_library.Sorting
{
  public class Counting : Sorting.Base
  {
    public int MaxValue { private get; set; }
    public Array Counter { private get; set; }

    public Counting(int[] array) : base(array) { }

    public override void Sort()
    {
      for (int i = 0; i < Array.Length; i++)
      {
        Counter.SetValue((int) Counter.GetValue(Array[i]) + 1, Array[i]);
      }

      int index = 0;
      for (int i = 0; i <= MaxValue; i++)
      {
        for (int j = 0; j < (int) Counter.GetValue(i); j++)
        {
          Array[index] = i;
          ++index;
        }
      }
    }
  }
}