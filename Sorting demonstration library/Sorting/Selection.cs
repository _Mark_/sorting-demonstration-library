﻿namespace Sorting_demonstration_library.Sorting
{
  public class Selection : Sorting.Base
  {
    public Selection(int[] array) : base(array) { }

    public override void Sort()
    {
      var index = 0;
      while (index < Array.Length)
      {
        var min = index;

        for (int i = index; i < Array.Length; ++i)
        {
          if (Array[min] > Array[i])
          {
            min = i;
          }
        }

        Swap(index, min);
        ++index;
      }
    }
  }
}