﻿namespace Sorting_demonstration_library.Sorting
{
  public class Bubble : Sorting.Base
  {
    public Bubble(int[] array) : base(array) { }

    public override void Sort()
    {
      var smallest = -1;
      var largest = Array.Length;

      while (smallest < largest)
      {
        var index = smallest + 2;
        while (index < largest)
        {
          if (Array[index - 1] > Array[index])
          {
            Swap(index - 1, index);
          }
          ++index;
        }
        --largest;

        index = largest - 2;
        while (index > smallest)
        {
          if (Array[index] > Array[index + 1])
          {
            Swap(index, index + 1);
          }
          --index;
        }
        ++smallest;
      }
    }
  }
}