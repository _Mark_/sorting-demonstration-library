namespace Sorting_demonstration_library.Sorting
{
  public class Quick : Sorting.Base
  {
    public Quick(int[] array) : base(array) { }
    
    public override void Sort()
    {
      Sort(0, Array.Length - 1);
    }

    private void Sort(int start, int end)
    {
      if (start >= end) return;

      var divider = Array[start];
      var left = start;
      var right = end;

      while (true)
      {
        FindSmaller(divider, ref left, ref right);

        if (left >= right) break;

        FindBigger(divider, ref left, ref right);

        if (left >= right) break;
      }

      Sort(start, left);
      Sort(right + 1, end);
    }

    private void FindSmaller(int divider, ref int left, ref int right)
    {
      while (true)
      {
        if (divider <= Array[right])
        {
          --right;
        }
        else
        {
          Swap(left, right);
          return;
        }

        if (left >= right) return;
      }
    }

    private void FindBigger(int divider, ref int left, ref int right)
    {
      while (true)
      {
        if (divider >= Array[left])
        {
          ++left;
        }
        else
        {
          Swap(left, right);
          return;
        }

        if (left >= right) return;
      }
    }
  }
}