﻿namespace Sorting_demonstration_library
{
  public static class Test
  {
    public static bool IsSequenceCorrect(int[] array)
    {
      for (int index = 1; index < array.Length; ++index)
      {
        if (array[index] < array[index - 1])
          return false;
      }

      return true;
    }
  }
}